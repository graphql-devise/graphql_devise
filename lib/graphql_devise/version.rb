# frozen_string_literal: true

module GraphqlDevise
  VERSION = '1.2.0'
end
